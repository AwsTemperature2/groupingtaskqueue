using System;
using Xunit;
using Amazon.Lambda.TestUtilities;
using Temperature.GroupingTaskQueue.Domain;
using System.Threading.Tasks;

namespace Temperature.GroupingTaskQueue.IntegrationTests
{
    public class FunctionIntegrationTest
    {
        public FunctionIntegrationTest()
        {
            Environment.SetEnvironmentVariable(LambdaBase.EnvironmentVariable, "Development");
        }

        [Fact]
        public async Task TestEnqueue()
        {
            var context = new TestLambdaContext();
            var request = new EnqueueTaskRequest
            {
                Dates = new DateTime[] { DateTime.Now }
            };

            Functions functions = new Functions();
            
            var response = await functions.EnqueueTasks(request, context);
            Assert.NotNull(response);
        }

        [Fact]
        public async Task TestDequeueAndRun()
        {
            var context = new TestLambdaContext();
            var request = new EnqueueTaskRequest
            {
                Dates = new DateTime[] { DateTime.Now }
            };

            Functions functions = new Functions();

            var response = await functions.DequeueAndRun(context);
            Assert.NotNull(response);
        }

        [Fact]
        public async Task TestStartProcessing()
        {
            var context = new TestLambdaContext();
            

            Functions functions = new Functions();

            await functions.StartQueueProcessing(context);
        }
    }
}
