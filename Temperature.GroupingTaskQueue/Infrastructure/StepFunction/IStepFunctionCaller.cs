﻿using System.Threading.Tasks;

namespace Temperature.GroupingTaskQueue.Infrastructure.StepFunction
{
    public interface IStepFunctionCaller
    {
        Task<string> CallStepFunction(string payload, string stepFunctionArn, string executionName);
    }
}
