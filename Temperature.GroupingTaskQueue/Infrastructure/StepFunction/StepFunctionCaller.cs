﻿using Amazon.StepFunctions;
using Amazon.StepFunctions.Model;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Temperature.GroupingTaskQueue.Infrastructure.StepFunction
{
    public class StepFunctionCaller : IStepFunctionCaller
    {
        private IAmazonStepFunctions stepFunction;

        public StepFunctionCaller(IAmazonStepFunctions stepFunction)
        {
            this.stepFunction = stepFunction ?? throw new ArgumentNullException(nameof(stepFunction));
        }

        // TODO use injected configuration 
        public async Task<string> CallStepFunction(string payload, string stepFunctionArn, string executionName)
        {
            var startExecutionRequest = new StartExecutionRequest()
            {
                Input = JsonConvert.SerializeObject(new { key = payload }),
                StateMachineArn = stepFunctionArn, // todo from config by environment
                Name = executionName
            };

            var response = await this.stepFunction.StartExecutionAsync(startExecutionRequest);
            Console.WriteLine($"Execution started with arn: {response.ExecutionArn}");
            return response.ExecutionArn;
        }
    }
}
