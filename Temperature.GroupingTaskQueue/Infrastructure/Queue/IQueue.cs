﻿using System.Threading.Tasks;

namespace Temperature.GroupingTaskQueue.Infrastructure.Queue
{
    public interface IQueue<T>
    {
        Task<QueueItem<T>> Dequeue();
        Task Enqueue(T item);
        Task Delete(string handle);
    }
}
