﻿namespace Temperature.GroupingTaskQueue.Infrastructure.Queue
{
    public class QueueItem<T>
    {
        public T Item { get; set; }
        public string Handle { get; set; }
    }
}
