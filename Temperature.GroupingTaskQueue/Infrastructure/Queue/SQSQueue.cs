﻿using Amazon.SQS;
using Amazon.SQS.Model;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;
using Temperature.GroupingTaskQueue.Infrastructure.Options;

namespace Temperature.GroupingTaskQueue.Infrastructure.Queue
{
    public class SQSQueue<T> : IQueue<T>
    {
        private readonly IAmazonSQS sqsClient;
        private readonly QueueOptions options;

        public SQSQueue(IAmazonSQS sqsClient, IOptions<QueueOptions> options)
        {
            this.sqsClient = sqsClient ?? throw new ArgumentNullException(nameof(sqsClient));
            this.options = options.Value ?? throw new ArgumentNullException(nameof(options));
        }


        public async Task<QueueItem<T>> Dequeue()
        {
            var receiveRequest = new ReceiveMessageRequest()
            {
                QueueUrl = options.QueueUrl,
                MaxNumberOfMessages = 1
            };

            var response = await sqsClient.ReceiveMessageAsync(receiveRequest);

            var message = response.Messages.FirstOrDefault();

            if (message == null)
            {
                return null;
            }

            var body = message.Body ?? string.Empty;
            var handle = message.ReceiptHandle ?? string.Empty;

            return new QueueItem<T>
            {
                Item = JsonConvert.DeserializeObject<T>(body),
                Handle = handle
            };
        }

        public async Task Enqueue(T item)
        {
            var sendRequest = new SendMessageRequest()
            {
                QueueUrl = options.QueueUrl,
                DelaySeconds = 5,
                MessageBody = JsonConvert.SerializeObject(item)
            };

            var response = await sqsClient.SendMessageAsync(sendRequest);

            // todo check if sending fails
        }

        public async Task Delete(string handle)
        {

            var deleteRequest = new DeleteMessageRequest()
            {
                QueueUrl = options.QueueUrl,
                ReceiptHandle = handle
            };

            var response = await sqsClient.DeleteMessageAsync(deleteRequest);

            // todo handle error
        }
    }
}
