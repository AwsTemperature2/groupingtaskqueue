namespace Temperature.GroupingTaskQueue.Infrastructure.Options
{
    public class QueueOptions
    {
        public string QueueUrl { get; set; }
    }
}
