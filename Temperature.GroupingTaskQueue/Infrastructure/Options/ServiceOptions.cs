using System.Collections.Generic;

namespace Temperature.GroupingTaskQueue.Infrastructure.Options
{
    public class ServiceOptions
    {
        public List<int> Intervals { get; set; }
        public string QueueUrl { get; set; }
        public string GroupingLambdaName { get; set; }
    }
}
