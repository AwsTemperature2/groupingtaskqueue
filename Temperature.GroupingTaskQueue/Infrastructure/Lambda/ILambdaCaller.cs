﻿using System.Threading.Tasks;
using Temperature.GroupingTaskQueue.Infrastructure.Options;

namespace Temperature.GroupingTaskQueue.Infrastructure.Lambda
{
    public interface ILambdaCaller
    {
        Task<string> CallLambda(object payload);
        Task<TResponse> GetLambdaResponse<TResponse>(object payload);
    }
}
