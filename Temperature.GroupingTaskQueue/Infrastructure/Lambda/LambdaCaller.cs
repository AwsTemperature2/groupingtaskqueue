﻿using Amazon.Lambda;
using Amazon.Lambda.Model;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading.Tasks;
using Temperature.GroupingTaskQueue.Infrastructure.Options;

namespace Temperature.GroupingTaskQueue.Infrastructure.Lambda
{
    public class LambdaCaller : ILambdaCaller
    {
        private readonly IAmazonLambda client;
        private readonly ServiceOptions lambdaOptions;

        public LambdaCaller(IAmazonLambda client, IOptions<ServiceOptions> options)
        {
            this.client = client ?? throw new ArgumentNullException(nameof(client));
            this.lambdaOptions = options.Value ?? throw new ArgumentNullException(nameof(options));
        }

        public async Task<string> CallLambda(object payload)
        {
            var invokeRequest = new InvokeRequest
            {
                FunctionName = lambdaOptions.GroupingLambdaName,
                InvocationType = InvocationType.RequestResponse,
                Payload = JsonConvert.SerializeObject(payload)
            };

            var response = await client.InvokeAsync(invokeRequest);
            using (var sr = new StreamReader(response.Payload))
            {
                var content = await sr.ReadToEndAsync();
                return content;
            }
        }

        public async Task<TResponse> GetLambdaResponse<TResponse>(object payload)
        {
            var resultString = await CallLambda(payload);

            return JsonConvert.DeserializeObject<TResponse>(resultString);
        }
    }

}
