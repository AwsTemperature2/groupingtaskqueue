using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.IO;

namespace Temperature.GroupingTaskQueue
{
    public abstract class LambdaBase
    {
        public const string EnvironmentVariable = "ASPNETCORE_ENVIRONMENT";

        public static IConfigurationRoot Configuration { get; private set; }
        private readonly IServiceProvider serviceProvider;

        protected LambdaBase()
        {
            SetupConfiguration();

            // then setup container basics
            var services = new ServiceCollection()
                .AddLogging()
                .AddOptions();

            this.SetupBaseServices(services);
            this.ConfigureServices(services);
            this.InjectOptions(services);

            this.serviceProvider = services.BuildServiceProvider();

            this.ConfigureLogging(this.serviceProvider.GetService<ILoggerFactory>());
        }

        private static void SetupConfiguration()
        {
            var configurationBuilder = GetConfigurationBuilder();
            Configuration = configurationBuilder.Build();
        }

        private static IConfigurationBuilder GetConfigurationBuilder()
        {
            var directory = Directory.GetCurrentDirectory();
            var environmentName = Environment.GetEnvironmentVariable(EnvironmentVariable);

            var builder = new ConfigurationBuilder()
                .SetBasePath(directory)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environmentName}.json", optional: false, reloadOnChange: true);

            builder.AddEnvironmentVariables();
            return builder;
        }        

        protected virtual void ConfigureServices(IServiceCollection services)
        {
        }

        protected virtual void InjectOptions(IServiceCollection services)
        {
        }

        protected void SetupBaseServices(IServiceCollection services)
        {
            services.AddDefaultAWSOptions(Configuration.GetAWSOptions());
            services.AddAutoMapper(typeof(LambdaBase));
        }

        protected T GetService<T>() where T : class
        {
            return this.serviceProvider.GetService<T>();
        }

        private void ConfigureLogging(ILoggerFactory loggerFactory)
        {
            loggerFactory.AddLambdaLogger(Configuration.GetLambdaLoggerOptions());
        }
    }
}
