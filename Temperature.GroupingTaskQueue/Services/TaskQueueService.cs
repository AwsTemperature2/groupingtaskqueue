using AutoMapper;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Temperature.GroupingTaskQueue.Domain;
using Temperature.GroupingTaskQueue.Infrastructure.Lambda;
using Temperature.GroupingTaskQueue.Infrastructure.Options;
using Temperature.GroupingTaskQueue.Infrastructure.Queue;
using Temperature.GroupingTaskQueue.Infrastructure.StepFunction;

namespace Temperature.GroupingTaskQueue.Services
{
    public class TaskQueueService : ITaskQueueService
    {
        private readonly IQueue<GroupingTask> queue;
        private readonly ServiceOptions serviceOptions;
        private readonly ILambdaCaller lambdaCaller;
        private readonly IStepFunctionCaller stepFunction;

        public TaskQueueService(IQueue<GroupingTask> queue,
                                IOptions<ServiceOptions> serviceOptions,
                                ILambdaCaller lambdaCaller,
                                IStepFunctionCaller stepFunction)
        {
            this.queue = queue ?? throw new ArgumentNullException(nameof(queue));
            this.serviceOptions = serviceOptions.Value ?? throw new ArgumentNullException(nameof(serviceOptions));
            this.lambdaCaller = lambdaCaller ?? throw new ArgumentNullException(nameof(lambdaCaller));
            this.stepFunction = stepFunction ?? throw new ArgumentNullException(nameof(stepFunction));
        }

        public async Task Enqueue(EnqueueTaskRequest request)
        {
            // todo introduce batch enqueue

            foreach (DateTime d in request.Dates)
            {
                foreach (var interval in serviceOptions.Intervals)
                {
                    await queue.Enqueue(new GroupingTask
                    {
                        Date = d,
                        Interval = interval
                    });
                }
            };
        }
        
        public async Task<DequeueAndRunResult> DequeueAndRun()
        {
            var task = await queue.Dequeue();

            if (task == null)
            {
                Console.WriteLine("Queue is empty, there is nothing to do"); // todo inject logger
                return new DequeueAndRunResult { ProcessedItems = 0 }; // todo map
            }

            Console.WriteLine("Running task:");
            Console.WriteLine(JsonConvert.SerializeObject(task));

            await lambdaCaller.CallLambda(new { Interval = task.Item.Interval, Days = new[] { task.Item.Date } }); // todo mapper

            await queue.Delete(task.Handle);
            return new DequeueAndRunResult { ProcessedItems = 1 }; // todo map
        }

        public async Task StartQueueProcessing()
        {
            // yes, it needs to be configurable...
            await stepFunction.CallStepFunction(string.Empty, "arn:aws:states:eu-central-1:358786124023:stateMachine:ProcessGroupingTasksQueue-Dev", "ProcessQueueDev-" + DateTime.Now.Ticks);
        }

        public async Task RunGroupingTasks(IEnumerable<GroupingTask> tasks)
        {
            foreach (var task in tasks)
            {
                await lambdaCaller.CallLambda(new { Interval = task.Interval, Days = new[] { task.Date } }); // todo mapper
            }
        }
    }
}
