cd..
dotnet restore
dotnet lambda deploy-serverless temperature-grouping-queue-prod --region eu-central-1 --profile default -tp "EnvironmentName=Production"
cd Deployment
pause
