using System.Linq;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.Lambda;
using Amazon.Lambda.Core;
using Amazon.Lambda.SQSEvents;
using Amazon.SQS;
using Amazon.StepFunctions;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Temperature.GroupingTaskQueue.Domain;
using Temperature.GroupingTaskQueue.Infrastructure.Lambda;
using Temperature.GroupingTaskQueue.Infrastructure.Options;
using Temperature.GroupingTaskQueue.Infrastructure.Queue;
using Temperature.GroupingTaskQueue.Infrastructure.StepFunction;
using Temperature.GroupingTaskQueue.Services;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace Temperature.GroupingTaskQueue
{
    public class Functions : LambdaBase
    {
        public Functions()
        {

        }

        protected override void ConfigureServices(IServiceCollection services)
        {
            services.AddAWSService<IAmazonDynamoDB>();
            services.AddAWSService<IAmazonSQS>();
            services.AddAWSService<IAmazonLambda>();
            services.AddAWSService<IAmazonStepFunctions>();
            services.AddTransient<ITaskQueueService, TaskQueueService>();
            services.AddTransient<IQueue<GroupingTask>, SQSQueue<GroupingTask>>();
            services.AddTransient<ILambdaCaller, LambdaCaller>();
            services.AddTransient<IStepFunctionCaller, StepFunctionCaller>();
        }

        protected override void InjectOptions(IServiceCollection services)
        {
            services.Configure<ServiceOptions>(Configuration.GetSection("ServiceOptions"));
            services.Configure<QueueOptions>(Configuration.GetSection("QueueOptions"));
        }

        public async Task<string> EnqueueTasks(EnqueueTaskRequest request, ILambdaContext context)
        {
            var service = this.GetService<ITaskQueueService>();
            await service.Enqueue(request);
            return "Tasks enqueued";
        }

        public async Task<DequeueAndRunResult> DequeueAndRun(ILambdaContext context)
        {
            var service = this.GetService<ITaskQueueService>();
            return await service.DequeueAndRun();            
        }

        public async Task SqsHandler(SQSEvent sqsEvent, ILambdaContext context)
        {
            var service = this.GetService<ITaskQueueService>();
            var tasks = sqsEvent.Records.Select(r => JsonConvert.DeserializeObject<GroupingTask>(r.Body));
            await service.RunGroupingTasks(tasks);
        }

        public async Task StartQueueProcessing(ILambdaContext context)
        {
            var service = this.GetService<ITaskQueueService>();
            await service.StartQueueProcessing();
        }
    }
}
