using System.Collections.Generic;
using System.Threading.Tasks;
using Temperature.GroupingTaskQueue.Domain;

namespace Temperature.GroupingTaskQueue
{
    public interface ITaskQueueService
    {
        Task Enqueue(EnqueueTaskRequest request);

        /// <summary>
        /// Dequeue item and process it
        /// </summary>
        /// <returns>Number of processed queue items</returns>
        Task<DequeueAndRunResult> DequeueAndRun();

        Task StartQueueProcessing();

        Task RunGroupingTasks(IEnumerable<GroupingTask> task);
    }
}
