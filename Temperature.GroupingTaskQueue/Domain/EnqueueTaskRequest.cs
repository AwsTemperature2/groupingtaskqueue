﻿using System;

namespace Temperature.GroupingTaskQueue.Domain
{
    public class EnqueueTaskRequest
    {
        public DateTime[] Dates { get; set; }
    }
}
