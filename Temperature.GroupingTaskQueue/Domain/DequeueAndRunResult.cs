﻿namespace Temperature.GroupingTaskQueue.Domain
{
    public class DequeueAndRunResult
    {
        public int ProcessedItems { get; set; }
    }
}
