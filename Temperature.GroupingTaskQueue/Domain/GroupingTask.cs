﻿using System;

namespace Temperature.GroupingTaskQueue.Domain
{
    public class GroupingTask
    {
        public DateTime Date { get; set; }
        public int Interval { get; set; }
    }
}
